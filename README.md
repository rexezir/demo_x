# spacexmissions

SpaceX missions.

## Getting Started

[Install Flutter](https://flutter.dev/docs/get-started/install)

Clone git repo in usual way.

Open project folder in Android Studio or VS Code.

Start Android Emulator or iOS Simulator. 
- [Running and debugging Flutter project](https://flutter.dev/docs/development/tools/android-studio#running-and-debugging)

Select "main.dart" in Run configuration to start app.
Select tests.dart to start widget and bloc tests.

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
