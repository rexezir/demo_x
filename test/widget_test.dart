import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:spacexmissions/blocs/mission_search/mission_search_bloc.dart';
import 'package:spacexmissions/blocs/theme/theme_bloc.dart';
import 'package:spacexmissions/generated/l10n.dart';
import 'package:spacexmissions/locator.dart';
import 'package:spacexmissions/providers/repository_provider.dart';
import 'package:spacexmissions/repositories/mission_repository.dart';
import 'package:spacexmissions/widgets/screens/search_screen_widget.dart';

void widgetTests() {
  locator.registerSingleton<AppRepositoryProvider>(
      AppRepositoryProvider(missionRepository: MockMissionRepository()));

  group('Widget tests', () {
    testWidgets('Initial app state', (WidgetTester tester) async {
      await tester.pumpWidget(BlocProvider<ThemeBloc>(
        create: (context) => ThemeBloc(),
        child: BlocProvider(
            create: (context) => MissionSearchBloc(
                repository: locator<AppRepositoryProvider>().missionRepository,
                globalVars: locator<AppRepositoryProvider>().globalVars),
            child: MaterialApp(
              supportedLocales: S.delegate.supportedLocales,
              localizationsDelegates: [
                S.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              home: SearchScreenWidget(),
            )),
      ));

      await tester.pump();

      expect(
          find.byKey(
            Key('text_field'),
          ),
          findsOneWidget,
          reason: "Text field exists");
      expect(
          find.byKey(
            Key('items_List'),
          ),
          findsNothing,
          reason: "No search result list");
      expect(
          find.byKey(
            Key("clear_text_button"),
          ),
          findsOneWidget,
          reason: "Clear Button exists");

      final finder = find.byKey(Key("clear_text_button_opacity"));
      expect(finder, findsOneWidget);
      final widget = tester.firstWidget(finder) as AnimatedOpacity;
      expect(widget.opacity, equals(0.0), reason: "Clear Button opacity 0.0");
    });

    testWidgets('Show search results', (WidgetTester tester) async {
      final searchString = "123";

      final resultCount =
          (await MockMissionRepository().searchMission(searchString, 0))
              .launches
              .length;

      await tester.pumpWidget(BlocProvider<ThemeBloc>(
        create: (context) => ThemeBloc(),
        child: BlocProvider(
            create: (context) => MissionSearchBloc(
                repository: locator<AppRepositoryProvider>().missionRepository,
                globalVars: locator<AppRepositoryProvider>().globalVars),
            child: MaterialApp(
              supportedLocales: S.delegate.supportedLocales,
              localizationsDelegates: [
                S.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              home: SearchScreenWidget(),
            )),
      ));

      await tester.pump();

      await tester.enterText(
          find.byKey(
            Key('text_field'),
          ),
          "1");

      await tester.pump();

      final finder = find.byKey(Key("clear_text_button_opacity"));
      expect(finder, findsOneWidget);
      final widget = tester.firstWidget(finder) as AnimatedOpacity;
      expect(widget.opacity, equals(1.0), reason: "Clear Button opacity 1.0");

      expect(
          find.byKey(
            Key('items_List'),
          ),
          findsNothing,
          reason: "No search result list");

      await tester.enterText(
          find.byKey(
            Key('text_field'),
          ),
          searchString);

      await tester.testTextInput.receiveAction(TextInputAction.done);
      await tester.pump();

      final listFinder = find.byKey(
        Key('items_List'),
      );

      expect(listFinder, findsOneWidget, reason: "Search result list shown");

      final listWidget = tester.firstWidget(listFinder) as ListView;

      expect(listWidget.childrenDelegate.estimatedChildCount,
          equals(resultCount + 1));
    });
  });
}
