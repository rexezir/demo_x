import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:spacexmissions/blocs/mission_search/mission_search_bloc.dart';
import 'package:spacexmissions/providers/repository_provider.dart';
import 'package:spacexmissions/repositories/mission_repository.dart';

Future blocTests() async {
  final AppRepositoryProvider repoProvider =
      AppRepositoryProvider(missionRepository: MockMissionRepository());

  final mockMissions =
      (await repoProvider.missionRepository.searchMission("", 0)).launches;

  final searString = "123";

  group('Sample Block test', () {
    late MissionSearchBloc missionBloc;

    setUp(() {
      missionBloc = MissionSearchBloc(
          repository: repoProvider.missionRepository,
          globalVars: repoProvider.globalVars);
    });

    test('initial state', () {
      expect(missionBloc.state, MissionSearchState.initial());
    });

    blocTest(
      'type $searString, then search, then clear',
      build: () => missionBloc,
      act: (bloc) async {
        missionBloc.add(TextUpdateMissionSearchEvent(text: searString));
        missionBloc.add(RequestMissionSearchEvent());
        await Future.delayed(Duration(milliseconds: 100));
        missionBloc.add(TextUpdateMissionSearchEvent(text: ""));
      },
      expect: () => [
        MissionSearchState.initial().copyWith(textFieldValue: searString),
        MissionSearchState.initial()
            .copyWith(textFieldValue: searString, inRequest: true),
        MissionSearchState.initial().copyWith(
            textFieldValue: searString,
            inRequest: false,
            launches: mockMissions,
            lastRequestedValue: searString),
        MissionSearchState.initial().copyWith(
            textFieldValue: "",
            inRequest: false,
            launches: [],
            lastRequestedValue: null),
      ],
    );
  });
}
