import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:spacexmissions/models/mission_search_response.dart';
import 'package:spacexmissions/repositories/global_vars.dart';
import 'package:spacexmissions/repositories/mission_repository.dart';

part 'mission_search_bloc.freezed.dart';
part 'mission_search_event.dart';
part 'mission_search_state.dart';

class MissionSearchBloc extends Bloc<MissionSearchEvent, MissionSearchState> {
  MissionSearchBloc({required this.repository, required this.globalVars})
      : super(MissionSearchState.initial());

  final MissionRepository repository;
  final GlobalVars globalVars;

  @override
  Stream<MissionSearchState> mapEventToState(
    MissionSearchEvent event,
  ) async* {
    switch (event.runtimeType) {
      case (TextUpdateMissionSearchEvent):
        yield* _handleTextUpdate(event);
        break;
      case (RequestMissionSearchEvent):
        yield* _handleSearch();
        break;
      case (ResultReceivedMissionSearchEvent):
        yield* _handleSearchResult(event);
        break;
      case (NextPageMissionSearchEvent):
        yield* _handleNextPage(event);
        break;
    }
  }

  Stream<MissionSearchState> _handleTextUpdate(
      MissionSearchEvent event) async* {
    event = event as TextUpdateMissionSearchEvent;
    yield state.copyWith(
        textFieldValue: event.text,
        inRequest: false,
        launches: [],
        fetchError: null,
        lastRequestedValue: null);
  }

  Stream<MissionSearchState> _handleSearch() async* {
    if (state.textFieldValue.length < globalVars.missionSearchMinLen) {
      return;
    }

    yield state.copyWith(inRequest: true, fetchError: null);

    _requestPage(offset: 0);
  }

  Stream<MissionSearchState> _handleSearchResult(
      MissionSearchEvent event) async* {
    event = event as ResultReceivedMissionSearchEvent;

    if (state.textFieldValue != event.textFieldValue) {
      return;
    }

    final newLaunchList =
        event.lastPage == 0 ? <Launch>[] : state.launches.toList();
    newLaunchList.addAll(event.launches);

    yield state.copyWith(
        inRequest: false,
        launches: newLaunchList,
        lastRequestedValue: event.textFieldValue,
        fetchError: event.fetchError,
        hasMore: event.fetchError != null
            ? state.hasMore
            : event.launches.length == 10);
  }

  Stream<MissionSearchState> _handleNextPage(MissionSearchEvent event) async* {
    yield state.copyWith(inRequest: true, fetchError: null);

    _requestPage(offset: state.launches.length);
  }

  _requestPage({required int offset}) async {
    final textFieldValue = state.textFieldValue;
    final result = await repository.searchMission(state.textFieldValue, offset);

    add(ResultReceivedMissionSearchEvent(
        lastPage: offset,
        textFieldValue: textFieldValue,
        launches: result.launches,
        fetchError: result.fetchError));
  }
}
