// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'mission_search_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$MissionSearchStateTearOff {
  const _$MissionSearchStateTearOff();

  _MissionSearchState call(
      {required String textFieldValue,
      required List<Launch> launches,
      required int lastPage,
      required bool inRequest,
      required bool noMoreResults,
      required String? lastRequestedValue,
      required bool hasMore,
      required FetchError? fetchError}) {
    return _MissionSearchState(
      textFieldValue: textFieldValue,
      launches: launches,
      lastPage: lastPage,
      inRequest: inRequest,
      noMoreResults: noMoreResults,
      lastRequestedValue: lastRequestedValue,
      hasMore: hasMore,
      fetchError: fetchError,
    );
  }
}

/// @nodoc
const $MissionSearchState = _$MissionSearchStateTearOff();

/// @nodoc
mixin _$MissionSearchState {
  String get textFieldValue => throw _privateConstructorUsedError;
  List<Launch> get launches => throw _privateConstructorUsedError;
  int get lastPage => throw _privateConstructorUsedError;
  bool get inRequest => throw _privateConstructorUsedError;
  bool get noMoreResults => throw _privateConstructorUsedError;
  String? get lastRequestedValue => throw _privateConstructorUsedError;
  bool get hasMore => throw _privateConstructorUsedError;
  FetchError? get fetchError => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $MissionSearchStateCopyWith<MissionSearchState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MissionSearchStateCopyWith<$Res> {
  factory $MissionSearchStateCopyWith(
          MissionSearchState value, $Res Function(MissionSearchState) then) =
      _$MissionSearchStateCopyWithImpl<$Res>;
  $Res call(
      {String textFieldValue,
      List<Launch> launches,
      int lastPage,
      bool inRequest,
      bool noMoreResults,
      String? lastRequestedValue,
      bool hasMore,
      FetchError? fetchError});
}

/// @nodoc
class _$MissionSearchStateCopyWithImpl<$Res>
    implements $MissionSearchStateCopyWith<$Res> {
  _$MissionSearchStateCopyWithImpl(this._value, this._then);

  final MissionSearchState _value;
  // ignore: unused_field
  final $Res Function(MissionSearchState) _then;

  @override
  $Res call({
    Object? textFieldValue = freezed,
    Object? launches = freezed,
    Object? lastPage = freezed,
    Object? inRequest = freezed,
    Object? noMoreResults = freezed,
    Object? lastRequestedValue = freezed,
    Object? hasMore = freezed,
    Object? fetchError = freezed,
  }) {
    return _then(_value.copyWith(
      textFieldValue: textFieldValue == freezed
          ? _value.textFieldValue
          : textFieldValue // ignore: cast_nullable_to_non_nullable
              as String,
      launches: launches == freezed
          ? _value.launches
          : launches // ignore: cast_nullable_to_non_nullable
              as List<Launch>,
      lastPage: lastPage == freezed
          ? _value.lastPage
          : lastPage // ignore: cast_nullable_to_non_nullable
              as int,
      inRequest: inRequest == freezed
          ? _value.inRequest
          : inRequest // ignore: cast_nullable_to_non_nullable
              as bool,
      noMoreResults: noMoreResults == freezed
          ? _value.noMoreResults
          : noMoreResults // ignore: cast_nullable_to_non_nullable
              as bool,
      lastRequestedValue: lastRequestedValue == freezed
          ? _value.lastRequestedValue
          : lastRequestedValue // ignore: cast_nullable_to_non_nullable
              as String?,
      hasMore: hasMore == freezed
          ? _value.hasMore
          : hasMore // ignore: cast_nullable_to_non_nullable
              as bool,
      fetchError: fetchError == freezed
          ? _value.fetchError
          : fetchError // ignore: cast_nullable_to_non_nullable
              as FetchError?,
    ));
  }
}

/// @nodoc
abstract class _$MissionSearchStateCopyWith<$Res>
    implements $MissionSearchStateCopyWith<$Res> {
  factory _$MissionSearchStateCopyWith(
          _MissionSearchState value, $Res Function(_MissionSearchState) then) =
      __$MissionSearchStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {String textFieldValue,
      List<Launch> launches,
      int lastPage,
      bool inRequest,
      bool noMoreResults,
      String? lastRequestedValue,
      bool hasMore,
      FetchError? fetchError});
}

/// @nodoc
class __$MissionSearchStateCopyWithImpl<$Res>
    extends _$MissionSearchStateCopyWithImpl<$Res>
    implements _$MissionSearchStateCopyWith<$Res> {
  __$MissionSearchStateCopyWithImpl(
      _MissionSearchState _value, $Res Function(_MissionSearchState) _then)
      : super(_value, (v) => _then(v as _MissionSearchState));

  @override
  _MissionSearchState get _value => super._value as _MissionSearchState;

  @override
  $Res call({
    Object? textFieldValue = freezed,
    Object? launches = freezed,
    Object? lastPage = freezed,
    Object? inRequest = freezed,
    Object? noMoreResults = freezed,
    Object? lastRequestedValue = freezed,
    Object? hasMore = freezed,
    Object? fetchError = freezed,
  }) {
    return _then(_MissionSearchState(
      textFieldValue: textFieldValue == freezed
          ? _value.textFieldValue
          : textFieldValue // ignore: cast_nullable_to_non_nullable
              as String,
      launches: launches == freezed
          ? _value.launches
          : launches // ignore: cast_nullable_to_non_nullable
              as List<Launch>,
      lastPage: lastPage == freezed
          ? _value.lastPage
          : lastPage // ignore: cast_nullable_to_non_nullable
              as int,
      inRequest: inRequest == freezed
          ? _value.inRequest
          : inRequest // ignore: cast_nullable_to_non_nullable
              as bool,
      noMoreResults: noMoreResults == freezed
          ? _value.noMoreResults
          : noMoreResults // ignore: cast_nullable_to_non_nullable
              as bool,
      lastRequestedValue: lastRequestedValue == freezed
          ? _value.lastRequestedValue
          : lastRequestedValue // ignore: cast_nullable_to_non_nullable
              as String?,
      hasMore: hasMore == freezed
          ? _value.hasMore
          : hasMore // ignore: cast_nullable_to_non_nullable
              as bool,
      fetchError: fetchError == freezed
          ? _value.fetchError
          : fetchError // ignore: cast_nullable_to_non_nullable
              as FetchError?,
    ));
  }
}

/// @nodoc

class _$_MissionSearchState extends _MissionSearchState {
  const _$_MissionSearchState(
      {required this.textFieldValue,
      required this.launches,
      required this.lastPage,
      required this.inRequest,
      required this.noMoreResults,
      required this.lastRequestedValue,
      required this.hasMore,
      required this.fetchError})
      : super._();

  @override
  final String textFieldValue;
  @override
  final List<Launch> launches;
  @override
  final int lastPage;
  @override
  final bool inRequest;
  @override
  final bool noMoreResults;
  @override
  final String? lastRequestedValue;
  @override
  final bool hasMore;
  @override
  final FetchError? fetchError;

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _MissionSearchState &&
            (identical(other.textFieldValue, textFieldValue) ||
                const DeepCollectionEquality()
                    .equals(other.textFieldValue, textFieldValue)) &&
            (identical(other.launches, launches) ||
                const DeepCollectionEquality()
                    .equals(other.launches, launches)) &&
            (identical(other.lastPage, lastPage) ||
                const DeepCollectionEquality()
                    .equals(other.lastPage, lastPage)) &&
            (identical(other.inRequest, inRequest) ||
                const DeepCollectionEquality()
                    .equals(other.inRequest, inRequest)) &&
            (identical(other.noMoreResults, noMoreResults) ||
                const DeepCollectionEquality()
                    .equals(other.noMoreResults, noMoreResults)) &&
            (identical(other.lastRequestedValue, lastRequestedValue) ||
                const DeepCollectionEquality()
                    .equals(other.lastRequestedValue, lastRequestedValue)) &&
            (identical(other.hasMore, hasMore) ||
                const DeepCollectionEquality()
                    .equals(other.hasMore, hasMore)) &&
            (identical(other.fetchError, fetchError) ||
                const DeepCollectionEquality()
                    .equals(other.fetchError, fetchError)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(textFieldValue) ^
      const DeepCollectionEquality().hash(launches) ^
      const DeepCollectionEquality().hash(lastPage) ^
      const DeepCollectionEquality().hash(inRequest) ^
      const DeepCollectionEquality().hash(noMoreResults) ^
      const DeepCollectionEquality().hash(lastRequestedValue) ^
      const DeepCollectionEquality().hash(hasMore) ^
      const DeepCollectionEquality().hash(fetchError);

  @JsonKey(ignore: true)
  @override
  _$MissionSearchStateCopyWith<_MissionSearchState> get copyWith =>
      __$MissionSearchStateCopyWithImpl<_MissionSearchState>(this, _$identity);
}

abstract class _MissionSearchState extends MissionSearchState {
  const factory _MissionSearchState(
      {required String textFieldValue,
      required List<Launch> launches,
      required int lastPage,
      required bool inRequest,
      required bool noMoreResults,
      required String? lastRequestedValue,
      required bool hasMore,
      required FetchError? fetchError}) = _$_MissionSearchState;
  const _MissionSearchState._() : super._();

  @override
  String get textFieldValue => throw _privateConstructorUsedError;
  @override
  List<Launch> get launches => throw _privateConstructorUsedError;
  @override
  int get lastPage => throw _privateConstructorUsedError;
  @override
  bool get inRequest => throw _privateConstructorUsedError;
  @override
  bool get noMoreResults => throw _privateConstructorUsedError;
  @override
  String? get lastRequestedValue => throw _privateConstructorUsedError;
  @override
  bool get hasMore => throw _privateConstructorUsedError;
  @override
  FetchError? get fetchError => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$MissionSearchStateCopyWith<_MissionSearchState> get copyWith =>
      throw _privateConstructorUsedError;
}
