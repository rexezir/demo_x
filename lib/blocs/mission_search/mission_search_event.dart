part of 'mission_search_bloc.dart';

abstract class MissionSearchEvent extends Equatable {
  const MissionSearchEvent();

  @override
  List<Object?> get props => [];
}

class TextUpdateMissionSearchEvent extends MissionSearchEvent {
  final String text;

  TextUpdateMissionSearchEvent({required this.text});

  @override
  List<Object?> get props => [text];
}

class ResultReceivedMissionSearchEvent extends MissionSearchEvent {
  final String textFieldValue;
  final List<Launch> launches;
  final int lastPage;
  final FetchError? fetchError;

  ResultReceivedMissionSearchEvent(
      {required this.textFieldValue,
      required this.launches,
      required this.fetchError,
      required this.lastPage});

  @override
  List<Object?> get props => [textFieldValue, launches, lastPage];
}

class RequestMissionSearchEvent extends MissionSearchEvent {}

class NextPageMissionSearchEvent extends MissionSearchEvent {}
