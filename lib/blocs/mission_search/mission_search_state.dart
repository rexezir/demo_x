part of 'mission_search_bloc.dart';

@freezed
class MissionSearchState with _$MissionSearchState, EquatableMixin {
  const MissionSearchState._();
  const factory MissionSearchState({
    required String textFieldValue,
    required List<Launch> launches,
    required int lastPage,
    required bool inRequest,
    required bool noMoreResults,
    required String? lastRequestedValue,
    required bool hasMore,
    required FetchError? fetchError,
  }) = _MissionSearchState;

  factory MissionSearchState.initial() => MissionSearchState(
        textFieldValue: "",
        launches: [],
        lastPage: 0,
        inRequest: false,
        noMoreResults: true,
        lastRequestedValue: null,
        hasMore: false,
        fetchError: null,
      );

  @override
  List<Object?> get props => [
        textFieldValue,
        launches,
        lastPage,
        inRequest,
        noMoreResults,
        lastRequestedValue,
        hasMore,
        fetchError,
      ];
}
