part of 'theme_bloc.dart';

@freezed
class ThemeState with _$ThemeState, EquatableMixin {
  const ThemeState._();

  @JsonSerializable(explicitToJson: true)
  @_ColorConverter()
  const factory ThemeState({
    required Color color1,
    required Color color2,
    required Color color3,
    required Color color4,
    required Color color5,
    required Color color6,
    required Color color7,
    required Color color8,
  }) = _ThemeState;

  factory ThemeState.initial() => ThemeState(
        color1: Colors.black,
        color2: Color(0xFF616161),
        color3: Colors.grey,
        color4: Colors.green,
        color5: Colors.yellow,
        color6: Color(0xFFEF5350),
        color7: Colors.red,
        color8: Color(0xFFB71C1C),
      );

  @override
  List<Object?> get props => [
        color1,
        color2,
        color3,
        color4,
        color5,
        color6,
        color7,
        color8,
      ];

  factory ThemeState.fromJson(Map<String, dynamic> json) =>
      _$ThemeStateFromJson(json);
}
