// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'theme_bloc.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ThemeState _$_$_ThemeStateFromJson(Map<String, dynamic> json) {
  return _$_ThemeState(
    color1: const _ColorConverter().fromJson(json['color1'] as int),
    color2: const _ColorConverter().fromJson(json['color2'] as int),
    color3: const _ColorConverter().fromJson(json['color3'] as int),
    color4: const _ColorConverter().fromJson(json['color4'] as int),
    color5: const _ColorConverter().fromJson(json['color5'] as int),
    color6: const _ColorConverter().fromJson(json['color6'] as int),
    color7: const _ColorConverter().fromJson(json['color7'] as int),
    color8: const _ColorConverter().fromJson(json['color8'] as int),
  );
}

Map<String, dynamic> _$_$_ThemeStateToJson(_$_ThemeState instance) =>
    <String, dynamic>{
      'color1': const _ColorConverter().toJson(instance.color1),
      'color2': const _ColorConverter().toJson(instance.color2),
      'color3': const _ColorConverter().toJson(instance.color3),
      'color4': const _ColorConverter().toJson(instance.color4),
      'color5': const _ColorConverter().toJson(instance.color5),
      'color6': const _ColorConverter().toJson(instance.color6),
      'color7': const _ColorConverter().toJson(instance.color7),
      'color8': const _ColorConverter().toJson(instance.color8),
    };

ChangeThemeEvent _$ChangeThemeEventFromJson(Map<String, dynamic> json) {
  return ChangeThemeEvent(
    const _ColorConverter().fromJson(json['color1'] as int),
    const _ColorConverter().fromJson(json['color2'] as int),
    const _ColorConverter().fromJson(json['color3'] as int),
    const _ColorConverter().fromJson(json['color4'] as int),
    const _ColorConverter().fromJson(json['color5'] as int),
    const _ColorConverter().fromJson(json['color6'] as int),
    const _ColorConverter().fromJson(json['color7'] as int),
    const _ColorConverter().fromJson(json['color8'] as int),
  );
}

Map<String, dynamic> _$ChangeThemeEventToJson(ChangeThemeEvent instance) =>
    <String, dynamic>{
      'color1': const _ColorConverter().toJson(instance.color1),
      'color2': const _ColorConverter().toJson(instance.color2),
      'color3': const _ColorConverter().toJson(instance.color3),
      'color4': const _ColorConverter().toJson(instance.color4),
      'color5': const _ColorConverter().toJson(instance.color5),
      'color6': const _ColorConverter().toJson(instance.color6),
      'color7': const _ColorConverter().toJson(instance.color7),
      'color8': const _ColorConverter().toJson(instance.color8),
    };
