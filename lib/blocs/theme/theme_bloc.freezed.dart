// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'theme_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ThemeState _$ThemeStateFromJson(Map<String, dynamic> json) {
  return _ThemeState.fromJson(json);
}

/// @nodoc
class _$ThemeStateTearOff {
  const _$ThemeStateTearOff();

  _ThemeState call(
      {required Color color1,
      required Color color2,
      required Color color3,
      required Color color4,
      required Color color5,
      required Color color6,
      required Color color7,
      required Color color8}) {
    return _ThemeState(
      color1: color1,
      color2: color2,
      color3: color3,
      color4: color4,
      color5: color5,
      color6: color6,
      color7: color7,
      color8: color8,
    );
  }

  ThemeState fromJson(Map<String, Object> json) {
    return ThemeState.fromJson(json);
  }
}

/// @nodoc
const $ThemeState = _$ThemeStateTearOff();

/// @nodoc
mixin _$ThemeState {
  Color get color1 => throw _privateConstructorUsedError;
  Color get color2 => throw _privateConstructorUsedError;
  Color get color3 => throw _privateConstructorUsedError;
  Color get color4 => throw _privateConstructorUsedError;
  Color get color5 => throw _privateConstructorUsedError;
  Color get color6 => throw _privateConstructorUsedError;
  Color get color7 => throw _privateConstructorUsedError;
  Color get color8 => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ThemeStateCopyWith<ThemeState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ThemeStateCopyWith<$Res> {
  factory $ThemeStateCopyWith(
          ThemeState value, $Res Function(ThemeState) then) =
      _$ThemeStateCopyWithImpl<$Res>;
  $Res call(
      {Color color1,
      Color color2,
      Color color3,
      Color color4,
      Color color5,
      Color color6,
      Color color7,
      Color color8});
}

/// @nodoc
class _$ThemeStateCopyWithImpl<$Res> implements $ThemeStateCopyWith<$Res> {
  _$ThemeStateCopyWithImpl(this._value, this._then);

  final ThemeState _value;
  // ignore: unused_field
  final $Res Function(ThemeState) _then;

  @override
  $Res call({
    Object? color1 = freezed,
    Object? color2 = freezed,
    Object? color3 = freezed,
    Object? color4 = freezed,
    Object? color5 = freezed,
    Object? color6 = freezed,
    Object? color7 = freezed,
    Object? color8 = freezed,
  }) {
    return _then(_value.copyWith(
      color1: color1 == freezed
          ? _value.color1
          : color1 // ignore: cast_nullable_to_non_nullable
              as Color,
      color2: color2 == freezed
          ? _value.color2
          : color2 // ignore: cast_nullable_to_non_nullable
              as Color,
      color3: color3 == freezed
          ? _value.color3
          : color3 // ignore: cast_nullable_to_non_nullable
              as Color,
      color4: color4 == freezed
          ? _value.color4
          : color4 // ignore: cast_nullable_to_non_nullable
              as Color,
      color5: color5 == freezed
          ? _value.color5
          : color5 // ignore: cast_nullable_to_non_nullable
              as Color,
      color6: color6 == freezed
          ? _value.color6
          : color6 // ignore: cast_nullable_to_non_nullable
              as Color,
      color7: color7 == freezed
          ? _value.color7
          : color7 // ignore: cast_nullable_to_non_nullable
              as Color,
      color8: color8 == freezed
          ? _value.color8
          : color8 // ignore: cast_nullable_to_non_nullable
              as Color,
    ));
  }
}

/// @nodoc
abstract class _$ThemeStateCopyWith<$Res> implements $ThemeStateCopyWith<$Res> {
  factory _$ThemeStateCopyWith(
          _ThemeState value, $Res Function(_ThemeState) then) =
      __$ThemeStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {Color color1,
      Color color2,
      Color color3,
      Color color4,
      Color color5,
      Color color6,
      Color color7,
      Color color8});
}

/// @nodoc
class __$ThemeStateCopyWithImpl<$Res> extends _$ThemeStateCopyWithImpl<$Res>
    implements _$ThemeStateCopyWith<$Res> {
  __$ThemeStateCopyWithImpl(
      _ThemeState _value, $Res Function(_ThemeState) _then)
      : super(_value, (v) => _then(v as _ThemeState));

  @override
  _ThemeState get _value => super._value as _ThemeState;

  @override
  $Res call({
    Object? color1 = freezed,
    Object? color2 = freezed,
    Object? color3 = freezed,
    Object? color4 = freezed,
    Object? color5 = freezed,
    Object? color6 = freezed,
    Object? color7 = freezed,
    Object? color8 = freezed,
  }) {
    return _then(_ThemeState(
      color1: color1 == freezed
          ? _value.color1
          : color1 // ignore: cast_nullable_to_non_nullable
              as Color,
      color2: color2 == freezed
          ? _value.color2
          : color2 // ignore: cast_nullable_to_non_nullable
              as Color,
      color3: color3 == freezed
          ? _value.color3
          : color3 // ignore: cast_nullable_to_non_nullable
              as Color,
      color4: color4 == freezed
          ? _value.color4
          : color4 // ignore: cast_nullable_to_non_nullable
              as Color,
      color5: color5 == freezed
          ? _value.color5
          : color5 // ignore: cast_nullable_to_non_nullable
              as Color,
      color6: color6 == freezed
          ? _value.color6
          : color6 // ignore: cast_nullable_to_non_nullable
              as Color,
      color7: color7 == freezed
          ? _value.color7
          : color7 // ignore: cast_nullable_to_non_nullable
              as Color,
      color8: color8 == freezed
          ? _value.color8
          : color8 // ignore: cast_nullable_to_non_nullable
              as Color,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
@_ColorConverter()
class _$_ThemeState extends _ThemeState {
  const _$_ThemeState(
      {required this.color1,
      required this.color2,
      required this.color3,
      required this.color4,
      required this.color5,
      required this.color6,
      required this.color7,
      required this.color8})
      : super._();

  factory _$_ThemeState.fromJson(Map<String, dynamic> json) =>
      _$_$_ThemeStateFromJson(json);

  @override
  final Color color1;
  @override
  final Color color2;
  @override
  final Color color3;
  @override
  final Color color4;
  @override
  final Color color5;
  @override
  final Color color6;
  @override
  final Color color7;
  @override
  final Color color8;

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ThemeState &&
            (identical(other.color1, color1) ||
                const DeepCollectionEquality().equals(other.color1, color1)) &&
            (identical(other.color2, color2) ||
                const DeepCollectionEquality().equals(other.color2, color2)) &&
            (identical(other.color3, color3) ||
                const DeepCollectionEquality().equals(other.color3, color3)) &&
            (identical(other.color4, color4) ||
                const DeepCollectionEquality().equals(other.color4, color4)) &&
            (identical(other.color5, color5) ||
                const DeepCollectionEquality().equals(other.color5, color5)) &&
            (identical(other.color6, color6) ||
                const DeepCollectionEquality().equals(other.color6, color6)) &&
            (identical(other.color7, color7) ||
                const DeepCollectionEquality().equals(other.color7, color7)) &&
            (identical(other.color8, color8) ||
                const DeepCollectionEquality().equals(other.color8, color8)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(color1) ^
      const DeepCollectionEquality().hash(color2) ^
      const DeepCollectionEquality().hash(color3) ^
      const DeepCollectionEquality().hash(color4) ^
      const DeepCollectionEquality().hash(color5) ^
      const DeepCollectionEquality().hash(color6) ^
      const DeepCollectionEquality().hash(color7) ^
      const DeepCollectionEquality().hash(color8);

  @JsonKey(ignore: true)
  @override
  _$ThemeStateCopyWith<_ThemeState> get copyWith =>
      __$ThemeStateCopyWithImpl<_ThemeState>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_ThemeStateToJson(this);
  }
}

abstract class _ThemeState extends ThemeState {
  const factory _ThemeState(
      {required Color color1,
      required Color color2,
      required Color color3,
      required Color color4,
      required Color color5,
      required Color color6,
      required Color color7,
      required Color color8}) = _$_ThemeState;
  const _ThemeState._() : super._();

  factory _ThemeState.fromJson(Map<String, dynamic> json) =
      _$_ThemeState.fromJson;

  @override
  Color get color1 => throw _privateConstructorUsedError;
  @override
  Color get color2 => throw _privateConstructorUsedError;
  @override
  Color get color3 => throw _privateConstructorUsedError;
  @override
  Color get color4 => throw _privateConstructorUsedError;
  @override
  Color get color5 => throw _privateConstructorUsedError;
  @override
  Color get color6 => throw _privateConstructorUsedError;
  @override
  Color get color7 => throw _privateConstructorUsedError;
  @override
  Color get color8 => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$ThemeStateCopyWith<_ThemeState> get copyWith =>
      throw _privateConstructorUsedError;
}
