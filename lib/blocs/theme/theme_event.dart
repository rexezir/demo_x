part of 'theme_bloc.dart';

@immutable
abstract class ThemeEvent {}

@JsonSerializable(explicitToJson: true)
@_ColorConverter()
@JsonKey(includeIfNull: false)
class ChangeThemeEvent extends ThemeEvent {
  final Color color1;
  final Color color2;
  final Color color3;
  final Color color4;
  final Color color5;
  final Color color6;
  final Color color7;
  final Color color8;

  factory ChangeThemeEvent.fromJson(Map<String, dynamic> json) =>
      _$ChangeThemeEventFromJson(json);

  ChangeThemeEvent(this.color1, this.color2, this.color3, this.color4,
      this.color5, this.color6, this.color7, this.color8);
  Map<String, dynamic> toJson() => _$ChangeThemeEventToJson(this);
}

class _ColorConverter implements JsonConverter<Color, int> {
  const _ColorConverter();

  @override
  Color fromJson(int json) => Color(json);

  @override
  int toJson(Color object) => object.value;
}
