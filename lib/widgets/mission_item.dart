import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:spacexmissions/generated/l10n.dart';
import 'package:spacexmissions/models/mission_search_response.dart';
import 'package:spacexmissions/widgets/picture_pages.dart';
import 'package:spacexmissions/widgets/shared/theme_mixin.dart';
import 'package:substring_highlight/substring_highlight.dart';

class MissionItem extends StatefulWidget {
  final Launch mission;
  final String searchText;

  MissionItem({Key? key, required this.mission, required this.searchText})
      : super(key: key);

  @override
  _MissionItemState createState() => _MissionItemState();
}

class _MissionItemState extends State<MissionItem> with ThemeMixin {
  final dateFormat = DateFormat('dd/MM/yyyy HH:mm:s.ms');

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Card(
        child: ExpansionTile(
          subtitle: widget.mission.launchDateUtc != null
              ? Text(dateFormat.format(widget.mission.launchDateUtc!))
              : null,
          leading: _buildIcon(context),
          title: _buildTitle(context),
          children: [_buildDetails(context)],
        ),
      ),
    );
  }

  Widget? _buildIcon(BuildContext context) {
    final success = widget.mission.launchSuccess;

    if (success == null) {
      return Icon(
        Icons.not_interested,
        color: theme.color3,
      );
    }

    if (success) {
      return Icon(
        Icons.check,
        color: theme.color4,
      );
    } else {
      return Icon(
        Icons.cancel_outlined,
        color: Theme.of(context).errorColor,
      );
    }
  }

  Widget _buildTitle(BuildContext context) {
    final displayText =
        widget.mission.missionName ?? S.of(context).noMissionName;

    return SubstringHighlight(
      text: displayText,
      textStyle: Theme.of(context).textTheme.subtitle1!,
      term: widget.searchText,
      textStyleHighlight: TextStyle(backgroundColor: theme.color5),
    );
  }

  Widget _buildDetails(BuildContext context) {
    List<String> imageUrls = <String>[];

    if ((widget.mission.links?.flickrImages?.length ?? 0) > 0) {
      imageUrls.addAll(widget.mission.links!.flickrImages!);
    }

    final shipImages =
        widget.mission.ships?.where((e) => e.image != null).map((e) => e.image);

    if (shipImages != null && shipImages.length > 0) {
      imageUrls.addAll(shipImages.cast());
    }

    return Container(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            Text((widget.mission.details ?? "").isEmpty
                ? S.of(context).noDescription
                : widget.mission.details!),
            if (imageUrls.length > 0)
              Container(
                height: 220,
                child: PicturePages(
                  urls: imageUrls,
                  initialIndex: 0,
                  noDetailOpen: false,
                ),
              )
          ],
        ));
  }
}
