import 'package:flutter/cupertino.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:spacexmissions/routes.dart';
import 'package:spacexmissions/widgets/shared/page_image.dart';
import 'package:spacexmissions/widgets/shared/theme_mixin.dart';

class PicturePages extends StatefulWidget {
  const PicturePages(
      {Key? key,
      required this.urls,
      this.initialIndex = 0,
      this.noDetailOpen = true})
      : super(key: key);

  final List<String> urls;
  final int initialIndex;
  final bool noDetailOpen;

  @override
  _PicturePagesState createState() => _PicturePagesState();
}

class _PicturePagesState extends State<PicturePages> with ThemeMixin {
  late PageController _controller;

  int? _currentPage;

  @override
  void initState() {
    _controller = PageController(initialPage: widget.initialIndex);
    _currentPage = widget.initialIndex;
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  _pageChanged(int page) {
    _currentPage = page;
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(top: 5),
          child: SmoothPageIndicator(
            controller: _controller,
            count: widget.urls.length,
            axisDirection: Axis.horizontal,
            effect: SlideEffect(
                spacing: 3.0,
                radius: 4.0,
                dotWidth: 8.0,
                dotHeight: 8.0,
                paintStyle: PaintingStyle.stroke,
                strokeWidth: 0.5,
                dotColor: theme.color3.withOpacity(0.5),
                activeDotColor: theme.color1),
          ),
        ),
        Expanded(
          child: GestureDetector(
            onTap: widget.noDetailOpen
                ? null
                : () {
                    Navigator.of(context).pushNamed(Routes.gallery,
                        arguments: PicturePagesArgs(
                            urls: widget.urls,
                            initialIndex: _currentPage!,
                            noDetailOpen: true));
                  },
            child: Container(
              padding: EdgeInsets.only(top: 10),
              child: PageView.builder(
                  onPageChanged: _pageChanged,
                  controller: _controller,
                  itemCount: widget.urls.length,
                  itemBuilder: (context, index) {
                    return Hero(
                      tag: widget.urls[index],
                      child: PageImage(
                        url: widget.urls[index],
                        noScale: !widget.noDetailOpen,
                      ),
                    );
                  }),
            ),
          ),
        ),
      ],
    );
  }
}

class PicturePagesArgs {
  final List<String> urls;
  final int initialIndex;
  final bool noDetailOpen;

  PicturePagesArgs(
      {this.noDetailOpen = true, required this.urls, this.initialIndex = 0});
}
