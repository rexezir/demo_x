import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spacexmissions/blocs/theme/theme_bloc.dart';

mixin ThemeMixin<T extends StatefulWidget> on State<T> {
  late ThemeState theme;

  @mustCallSuper
  @override
  Widget build(BuildContext context) {
    theme = BlocProvider.of<ThemeBloc>(context).state;

    return Container();
  }
}
