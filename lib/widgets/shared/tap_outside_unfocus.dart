import 'package:flutter/material.dart';

class TapOutsideUnfocus extends StatelessWidget {
  final Widget child;
  final HitTestBehavior behavior;

  TapOutsideUnfocus({
    required this.child,
    this.behavior = HitTestBehavior.opaque,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        behavior: behavior,
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: this.child);
  }
}
