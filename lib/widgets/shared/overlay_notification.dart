import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:spacexmissions/blocs/theme/theme_bloc.dart';

class OverlayNotification extends StatelessWidget {
  final double value;
  final Widget child;
  final double beginOffset;
  final double endOffset;

  OverlayNotification(
      {Key? key,
      required this.value,
      required this.child,
      this.beginOffset = 0.0,
      this.endOffset = 20.0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = BlocProvider.of<ThemeBloc>(context).state;

    final mq = MediaQuery.of(context);

    Tween<Offset> tweenOffset = Tween<Offset>(
        begin: Offset(
            0, (mq.padding.top + beginOffset).clamp(0.0, double.infinity)),
        end: Offset(
            0, (mq.padding.top + endOffset).clamp(0.0, double.infinity)));

    Tween<double> tweenOpacity = Tween<double>(begin: 0, end: 1);

    return Transform.translate(
      offset: tweenOffset.transform(value),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Align(
          alignment: Alignment.topCenter,
          child: SlideDismissible(
            key: ValueKey(key),
            direction: DismissDirection.horizontal,
            child: Container(
              width: double.infinity,
              child: Material(
                type: MaterialType.transparency,
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                  child: BackdropFilter(
                    filter: ImageFilter.blur(
                      sigmaX: value * 20,
                      sigmaY: value * 20,
                    ),
                    child: Opacity(
                      opacity: tweenOpacity.transform(value),
                      child: Container(
                          decoration: BoxDecoration(
                              color: theme.color6.withOpacity(0.4),
                              border: Border.all(
                                  color: theme.color3.withOpacity(0.5))),
                          child: child),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
