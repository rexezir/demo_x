import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class PageImage extends StatefulWidget {
  final String url;
  final bool noScale;

  const PageImage({
    Key? key,
    required this.url,
    this.noScale = true,
  }) : super(key: key);

  @override
  _PageImageState createState() => _PageImageState();
}

class _PageImageState extends State<PageImage>
    with AutomaticKeepAliveClientMixin {
  BoxFit fit = BoxFit.scaleDown;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return GestureDetector(
      onDoubleTap:

           () {
        if  (widget.noScale) {
          return;
        }
              if (fit == BoxFit.scaleDown) {
                setState(() {
                  fit = BoxFit.cover;
                });
              } else if (fit == BoxFit.cover) {
                setState(() {
                  fit = BoxFit.scaleDown;
                });
              }
            },
      child: CachedNetworkImage(
        fit: fit,
        key: PageStorageKey(widget.url),
        imageUrl: widget.url,
        progressIndicatorBuilder: (context, url, downloadProgress) => Center(
          child: Container(
            height: 24,
            width: 24,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(
                  Theme.of(context).accentColor.withOpacity(0.5)),
              strokeWidth: 2,
            ),
          ),
        ),
        errorWidget: (context, url, error) => Icon(Icons.error),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
