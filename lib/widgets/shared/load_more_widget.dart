import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spacexmissions/blocs/mission_search/mission_search_bloc.dart';

class LoadMoreWidget extends StatelessWidget {
  final double height;

  const LoadMoreWidget({Key? key, this.height = 24}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MissionSearchBloc, MissionSearchState>(
      builder: (context, state) {
        return AnimatedSwitcher(
          duration: kThemeAnimationDuration,
          child: state.hasMore
              ? Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      height: height,
                      child: TextButton(
                        child: Text("Load More"),
                        onPressed: state.inRequest
                            ? null
                            : () {
                                BlocProvider.of<MissionSearchBloc>(context)
                                    .add(NextPageMissionSearchEvent());
                              },
                      ),
                    ),
                    if (state.inRequest)
                      Container(
                          height: height / 2,
                          width: height / 2,
                          child: CircularProgressIndicator())
                  ],
                )
              : Container(
                  key: Key("NoMore"),
                ),
        );
      },
    );
  }
}
