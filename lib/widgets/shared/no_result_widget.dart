import 'package:flutter/material.dart';
import 'package:spacexmissions/generated/l10n.dart';

class NoResultWidget extends StatelessWidget {
  const NoResultWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Text(S.of(context).noResultsFromServer),
    );
  }
}
