import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:spacexmissions/blocs/mission_search/mission_search_bloc.dart';
import 'package:spacexmissions/generated/l10n.dart';
import 'package:spacexmissions/locator.dart';
import 'package:spacexmissions/providers/repository_provider.dart';
import 'package:spacexmissions/repositories/in_app_notification.dart';
import 'package:spacexmissions/widgets/mission_item.dart';
import 'package:spacexmissions/widgets/shared/load_more_widget.dart';
import 'package:spacexmissions/widgets/shared/no_result_widget.dart';
import 'package:spacexmissions/widgets/shared/tap_outside_unfocus.dart';
import 'package:spacexmissions/widgets/shared/theme_mixin.dart';

class SearchScreenWidget extends StatefulWidget {
  const SearchScreenWidget({Key? key}) : super(key: key);

  @override
  _SearchScreenWidgetState createState() => _SearchScreenWidgetState();
}

class _SearchScreenWidgetState extends State<SearchScreenWidget>
    with ThemeMixin {
  TextEditingController _textEditingController = TextEditingController();
  FocusNode _focusNode = FocusNode();
  late MissionSearchBloc _searchBloc;

  @override
  void initState() {
    _textEditingController.text =
        BlocProvider.of<MissionSearchBloc>(context).state.textFieldValue;
    _textEditingController.addListener(_controllerToBlockUpdate);

    super.initState();
  }

  @override
  void didChangeDependencies() {
    _searchBloc = BlocProvider.of<MissionSearchBloc>(context);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _focusNode.dispose();
    _textEditingController.removeListener(_controllerToBlockUpdate);
    _textEditingController.dispose();
    super.dispose();
  }

  _controllerToBlockUpdate() {
    if (_searchBloc.state.textFieldValue != _textEditingController.text) {
      _searchBloc
          .add(TextUpdateMissionSearchEvent(text: _textEditingController.text));
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return TapOutsideUnfocus(
      child: Scaffold(
        appBar: AppBar(
          title: Theme(
            data: Theme.of(context).copyWith(
                textTheme: Theme.of(context).textTheme.copyWith(
                      headline6: TextStyle(
                        color:
                            Theme.of(context).primaryTextTheme.headline6!.color,
                        fontSize: 20,
                      ),
                    )),
            child: Container(
              height: 52,
              child: TextFormField(
                key: Key('text_field'),
                autocorrect: false,
                cursorColor: theme.color2,
                controller: _textEditingController,
                focusNode: _focusNode,
                style: Theme.of(context).textTheme.headline6,
                textInputAction: TextInputAction.search,
                keyboardType: TextInputType.visiblePassword,
                onEditingComplete: () {
                  _searchBloc.add(RequestMissionSearchEvent());
                  FocusScope.of(context).unfocus();
                },
                autovalidateMode: AutovalidateMode.always,
                validator: (s) {
                  final len = (s ?? "").length;
                  final minLimit = locator<AppRepositoryProvider>()
                      .globalVars
                      .missionSearchMinLen;

                  return len > 0 && len < minLimit
                      ? S.of(context).searchStringTooShort(minLimit)
                      : null;
                },
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.zero,
                  focusedErrorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  border: InputBorder.none,
                  hintText: "Type mission name",
                  hintStyle: TextStyle(
                    color: Theme.of(context).primaryTextTheme.caption!.color,
                    fontSize: 20,
                  ),
                ),
              ),
            ),
          ),
          actions: _buildActions(context),
        ),
        body: _buildBody(),
      ),
    );
  }

  List<Widget> _buildActions(BuildContext context) {
    return [
      BlocBuilder<MissionSearchBloc, MissionSearchState>(
        builder: (context, state) {
          return AnimatedOpacity(
            key: Key("clear_text_button_opacity"),
            opacity: state.textFieldValue.isNotEmpty ? 1.0 : 0.0,
            duration: Duration(milliseconds: 200),
            curve: Curves.easeInOutCubic,
            child: IconButton(
              key: Key("clear_text_button"),
              icon: Icon(Icons.clear),
              onPressed: () =>
                  _searchBloc.add(TextUpdateMissionSearchEvent(text: "")),
            ),
          );
        },
      )
    ];
  }

  Widget _buildBody() {
    return BlocConsumer<MissionSearchBloc, MissionSearchState>(
      listener: (context, state) {
        if (_searchBloc.state.textFieldValue != _textEditingController.text) {
          _textEditingController.text = _searchBloc.state.textFieldValue;
        }
        if (state.fetchError != null) {
          locator<AppRepositoryProvider>()
              .inAppNotification
              .showInAppNotification(S.of(context).errorText,
                  type: AppNotificationType.ERROR);
        }
      },
      builder: (context, state) {
        Widget content;

        if (state.inRequest && state.launches.length == 0) {
          content = Container(
              height:
                  locator<AppRepositoryProvider>().globalVars.defaultItemHeight,
              alignment: Alignment.center,
              child: CircularProgressIndicator());
        } else if (state.launches.length > 0) {
          content = Scrollbar(
            child: ListView.builder(
                key: Key('items_List'),
                itemCount: state.launches.length + 1,
                itemBuilder: (context, index) {
                  if (index >= state.launches.length) {
                    return Container(
                      padding: EdgeInsets.only(bottom: 20),
                      child: LoadMoreWidget(
                        height: locator<AppRepositoryProvider>()
                                .globalVars
                                .defaultItemHeight /
                            2,
                      ),
                    );
                  }
                  return MissionItem(
                    mission: state.launches[index],
                    searchText: state.lastRequestedValue!,
                  );
                }),
          );
        } else if (state.launches.length == 0 &&
            state.lastRequestedValue != null &&
            state.fetchError == null) {
          content = Container(
              height:
                  locator<AppRepositoryProvider>().globalVars.defaultItemHeight,
              child: NoResultWidget());
        } else {
          content = Container();
        }

        return AnimatedSwitcher(
          duration: kThemeAnimationDuration,
          child: content,
        );
      },
    );
  }
}
