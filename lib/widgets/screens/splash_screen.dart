import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spacexmissions/locator.dart';
import 'package:spacexmissions/providers/repository_provider.dart';
import 'package:spacexmissions/routes.dart';
import 'package:spring/spring.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Future.delayed(Duration(
            milliseconds: locator<AppRepositoryProvider>()
                .globalVars
                .splashScreenDelayMs))
        .then((value) =>
            Navigator.of(context).pushReplacementNamed(Routes.searchScreen));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: Colors.white,
      child: Container(
          padding: EdgeInsets.symmetric(horizontal: 50),
          alignment: Alignment(0.0, -0.5),
          child: Spring.slide(
            withFade: true,
            delay: Duration(milliseconds: 500),
            animDuration: Duration(milliseconds: 1000),
            animStatus: (AnimStatus status) {
              print(status);
            },
            child: Image.asset(
              'assets/spaceX.png',
            ),
            slideType: SlideType.slide_in_top,
          )),
    );
  }
}
