import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spacexmissions/widgets/picture_pages.dart';

class Gallery extends StatelessWidget {
  const Gallery(
      {Key? key,
      required this.urls,
      this.initialIndex = 0,
      this.noDetailOpen = true})
      : super(key: key);

  final List<String> urls;
  final int initialIndex;
  final bool noDetailOpen;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: PicturePages(
            urls: urls,
            initialIndex: initialIndex,
            noDetailOpen: noDetailOpen,
          ),
        ),
      ),
    );
  }
}
