import 'package:graphql/client.dart';
import 'package:spacexmissions/models/mission_search_response.dart';

abstract class MissionRepository {
  Future<MissionSearchResponse> searchMission(String searchString, int offset);
}

class MockMissionRepository implements MissionRepository {
  Future<MissionSearchResponse> searchMission(
      String searchString, int offset) async {
    return MissionSearchResponse(launches: [
      Launch(missionName: "Test Mission"),
      Launch(missionName: "Another Mission")
    ]);
  }
}

class RemoteMissionRepository implements MissionRepository {
  static const _url = 'https://api.spacex.land/graphql/';

  static const String searchLaunches = r'''
  query GetLaunches($nName: String, $nOffset: Int!) { 
  launches(find: {mission_name: $nName}, offset: $nOffset, limit: 10) {
    id
    mission_name
    details
    launch_date_utc
    launch_site {
      site_name
    }
    launch_success
    links {
      article_link
      flickr_images
    }
    rocket {
      rocket_name
      rocket_type
    }
    ships {
      image
    }
  }
  }
''';

  final GraphQLClient client = GraphQLClient(
    cache: GraphQLCache(),
    link: HttpLink(_url),
  );

  Future<MissionSearchResponse> searchMission(
      String searchString, int offset) async {
    final QueryOptions options = QueryOptions(
      document: gql(searchLaunches),
      variables: <String, dynamic>{
        'nName': searchString,
        'nOffset': offset,
      },
    );

    final QueryResult result = await client.query(options);

    if (result.hasException) {
      if (result.exception != null) {
        if (result.exception!.graphqlErrors.length > 0) {
          return MissionSearchResponse.withError(result.exception!.graphqlErrors
              .firstWhere((element) => element.message.length > 0)
              .message);
        } else if (result.exception?.linkException?.originalException
                ?.toString() !=
            null) {
          return MissionSearchResponse.withError(
              result.exception?.linkException?.originalException?.toString() ??
                  "");
        } else {
          MissionSearchResponse.withError("");
        }
      }
    }

    return MissionSearchResponse.fromJson(result.data);
  }
}
