import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:spacexmissions/blocs/theme/theme_bloc.dart';
import 'package:spacexmissions/widgets/shared/overlay_notification.dart';

class InAppNotification {
  static const Duration _notificationShowDuration =
      Duration(milliseconds: 5000);
  static const Duration _notificationOpenDuration = Duration(milliseconds: 300);
  static const Duration _notificationHideDuration = Duration(milliseconds: 150);

  static ValueKey _notificationKey = ValueKey("_notificationKey");

  OverlaySupportEntry? _lastNotification;

  removeNotification() {
    _lastNotification?.dismiss();
  }

  showInAppNotification(String infoText,
      {Duration? showDuration, AppNotificationType? type}) {
    _lastNotification = showOverlay((context, t) {
      return OverlayNotification(
        value: t,
        child: _buildNotification(context, infoText, type),
        beginOffset: -15,
        endOffset: 5,
      );
    },
        key: _notificationKey,
        curve: Curves.fastOutSlowIn,
        duration: showDuration ?? _notificationShowDuration,
        animationDuration: _notificationOpenDuration,
        reverseAnimationDuration: _notificationHideDuration);
  }

  Widget _buildNotification(
      BuildContext context, String message, AppNotificationType? type) {
    final theme = BlocProvider.of<ThemeBloc>(context).state;

    return Container(
      padding: EdgeInsets.all(20),
      color: theme.color7.withOpacity(0.1),
      child: IntrinsicHeight(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              height: 40,
              child: Padding(
                padding: EdgeInsets.only(
                  right: 16,
                ),
                child: Container(
                  height: 24,
                  width: 24,
                  child: Icon(
                    typeToResource(type),
                    color:
                        type == AppNotificationType.ERROR ? theme.color8 : null,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Container(
                alignment: Alignment.centerLeft,
                child: RichText(
                  maxLines: 6,
                  overflow: TextOverflow.ellipsis,
                  softWrap: false,
                  text: TextSpan(text: "", children: [
                    TextSpan(
                      text: message,
                    )
                  ]),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  IconData? typeToResource(AppNotificationType? type) {
    switch (type) {
      case AppNotificationType.NEWS:
        return Icons.mail;

      case AppNotificationType.WARN:
        return Icons.announcement_outlined;

      case AppNotificationType.ERROR:
        return Icons.error_outline;

      case AppNotificationType.SUCCESS:
        return Icons.check;

      case AppNotificationType.NONE:
      case null:
        return Icons.message;
    }
  }
}

enum AppNotificationType { NEWS, WARN, ERROR, SUCCESS, NONE }
