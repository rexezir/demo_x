// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mission_search_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MissionSearchResponse _$MissionSearchResponseFromJson(
    Map<String, dynamic> json) {
  return MissionSearchResponse(
    launches: (json['launches'] as List<dynamic>?)
        ?.map((e) => Launch.fromJson(e as Map<String, dynamic>))
        .toList(),
    fetchError: json['fetchError'] == null
        ? null
        : FetchError.fromJson(json['fetchError'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$MissionSearchResponseToJson(
        MissionSearchResponse instance) =>
    <String, dynamic>{
      'launches': instance.launches.map((e) => e.toJson()).toList(),
      'fetchError': instance.fetchError?.toJson(),
    };

Launch _$LaunchFromJson(Map<String, dynamic> json) {
  return Launch(
    id: json['id'] as String?,
    missionName: json['mission_name'] as String?,
    details: json['details'] as String?,
    launchDateUtc: json['launch_date_utc'] == null
        ? null
        : DateTime.parse(json['launch_date_utc'] as String),
    launchSite: json['launch_site'] == null
        ? null
        : LaunchSite.fromJson(json['launch_site'] as Map<String, dynamic>),
    launchSuccess: json['launch_success'] as bool?,
    links: json['links'] == null
        ? null
        : Links.fromJson(json['links'] as Map<String, dynamic>),
    rocket: json['rocket'] == null
        ? null
        : Rocket.fromJson(json['rocket'] as Map<String, dynamic>),
    ships: (json['ships'] as List<dynamic>?)
        ?.map((e) => Ships.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$LaunchToJson(Launch instance) => <String, dynamic>{
      'id': instance.id,
      'mission_name': instance.missionName,
      'details': instance.details,
      'launch_date_utc': instance.launchDateUtc?.toIso8601String(),
      'launch_site': instance.launchSite?.toJson(),
      'launch_success': instance.launchSuccess,
      'links': instance.links?.toJson(),
      'rocket': instance.rocket?.toJson(),
      'ships': instance.ships?.map((e) => e.toJson()).toList(),
    };

LaunchSite _$LaunchSiteFromJson(Map<String, dynamic> json) {
  return LaunchSite(
    siteName: json['site_name'] as String?,
  );
}

Map<String, dynamic> _$LaunchSiteToJson(LaunchSite instance) =>
    <String, dynamic>{
      'site_name': instance.siteName,
    };

Links _$LinksFromJson(Map<String, dynamic> json) {
  return Links(
    articleLink: json['article_link'] as String?,
    flickrImages: (json['flickr_images'] as List<dynamic>?)
        ?.map((e) => e as String)
        .toList(),
  );
}

Map<String, dynamic> _$LinksToJson(Links instance) => <String, dynamic>{
      'article_link': instance.articleLink,
      'flickr_images': instance.flickrImages,
    };

Rocket _$RocketFromJson(Map<String, dynamic> json) {
  return Rocket(
    rocketName: json['rocket_name'] as String?,
    rocketType: json['rocket_type'] as String?,
  );
}

Map<String, dynamic> _$RocketToJson(Rocket instance) => <String, dynamic>{
      'rocket_name': instance.rocketName,
      'rocket_type': instance.rocketType,
    };

Ships _$ShipsFromJson(Map<String, dynamic> json) {
  return Ships(
    image: json['image'] as String?,
    name: json['name'] as String?,
    model: json['model'] as String?,
  );
}

Map<String, dynamic> _$ShipsToJson(Ships instance) => <String, dynamic>{
      'image': instance.image,
      'model': instance.model,
      'name': instance.name,
    };

FetchError _$FetchErrorFromJson(Map<String, dynamic> json) {
  return FetchError(
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$FetchErrorToJson(FetchError instance) =>
    <String, dynamic>{
      'message': instance.message,
    };
