import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'mission_search_response.g.dart';

@JsonSerializable(explicitToJson: true)
class MissionSearchResponse {
  List<Launch> launches;
  FetchError? fetchError;

  MissionSearchResponse({List<Launch>? launches, this.fetchError})
      : launches = launches ?? [];

  factory MissionSearchResponse.fromJson(Map<String, dynamic>? json) {
    if (json == null) {
      return MissionSearchResponse();
    } else {
      return _$MissionSearchResponseFromJson(json);
    }
  }

  factory MissionSearchResponse.withError(String message) {
    return MissionSearchResponse(fetchError: FetchError(message: message));
  }

  Map<String, dynamic> toJson() => _$MissionSearchResponseToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Launch with EquatableMixin {
  String? id;
  @JsonKey(name: "mission_name")
  String? missionName;
  String? details;
  @JsonKey(name: "launch_date_utc")
  DateTime? launchDateUtc;
  @JsonKey(name: "launch_site")
  LaunchSite? launchSite;
  @JsonKey(name: "launch_success")
  bool? launchSuccess;
  Links? links;
  Rocket? rocket;
  List<Ships>? ships;

  Launch(
      {this.id,
      this.missionName,
      this.details,
      this.launchDateUtc,
      this.launchSite,
      this.launchSuccess,
      this.links,
      this.rocket,
      this.ships});

  factory Launch.fromJson(Map<String, dynamic> json) => _$LaunchFromJson(json);
  Map<String, dynamic> toJson() => _$LaunchToJson(this);

  @override
  List<Object?> get props => [
        missionName,
        id,
        details,
        launchDateUtc,
        launchSite,
        launchSuccess,
        links,
        rocket,
        ships
      ];
}

@JsonSerializable(explicitToJson: true)
class LaunchSite {
  @JsonKey(name: "site_name")
  String? siteName;

  LaunchSite({this.siteName});

  factory LaunchSite.fromJson(Map<String, dynamic> json) =>
      _$LaunchSiteFromJson(json);
  Map<String, dynamic> toJson() => _$LaunchSiteToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Links {
  @JsonKey(name: "article_link")
  String? articleLink;
  @JsonKey(name: "flickr_images")
  List<String>? flickrImages;

  Links({this.articleLink, this.flickrImages});

  factory Links.fromJson(Map<String, dynamic> json) => _$LinksFromJson(json);
  Map<String, dynamic> toJson() => _$LinksToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Rocket {
  @JsonKey(name: "rocket_name")
  String? rocketName;
  @JsonKey(name: "rocket_type")
  String? rocketType;

  Rocket({this.rocketName, this.rocketType});

  factory Rocket.fromJson(Map<String, dynamic> json) => _$RocketFromJson(json);
  Map<String, dynamic> toJson() => _$RocketToJson(this);
}

@JsonSerializable(explicitToJson: true)
class Ships {
  String? image;
  String? model;
  String? name;

  Ships({this.image, this.name, this.model});

  factory Ships.fromJson(Map<String, dynamic> json) => _$ShipsFromJson(json);
  Map<String, dynamic> toJson() => _$ShipsToJson(this);
}

@JsonSerializable(explicitToJson: true)
class FetchError {
  final String message;

  FetchError({required this.message});

  factory FetchError.fromJson(Map<String, dynamic> json) =>
      _$FetchErrorFromJson(json);
  Map<String, dynamic> toJson() => _$FetchErrorToJson(this);
}
