import 'package:flutter/material.dart';
import 'package:spacexmissions/repositories/global_vars.dart';
import 'package:spacexmissions/repositories/in_app_notification.dart';
import 'package:spacexmissions/repositories/mission_repository.dart';

class AppRepositoryProvider with ChangeNotifier {
  late MissionRepository _missionRepository;

  MissionRepository get missionRepository => _missionRepository;

  late GlobalVars _globalVars;

  GlobalVars get globalVars => _globalVars;

  late InAppNotification _inAppNotification;

  InAppNotification get inAppNotification => _inAppNotification;

  AppRepositoryProvider(
      {MissionRepository? missionRepository, GlobalVars? globalVars}) {
    _missionRepository = missionRepository ?? RemoteMissionRepository();
    _globalVars = globalVars ?? GlobalVars();
    _inAppNotification = InAppNotification();
  }

  void setMissionRepository(MissionRepository newMissionRepository) {
    _missionRepository = newMissionRepository;
    notifyListeners();
  }

  void setGlobalVars(GlobalVars newGlobalVars) {
    _globalVars = newGlobalVars;
    notifyListeners();
  }
}
