import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:provider/provider.dart';
import 'package:spacexmissions/blocs/mission_search/mission_search_bloc.dart';
import 'package:spacexmissions/blocs/theme/theme_bloc.dart';
import 'package:spacexmissions/generated/l10n.dart';
import 'package:spacexmissions/locator.dart';
import 'package:spacexmissions/providers/repository_provider.dart';
import 'package:spacexmissions/route_transitions.dart';
import 'package:spacexmissions/routes.dart';
import 'package:spacexmissions/widgets/picture_pages.dart';
import 'package:spacexmissions/widgets/screens/gallery.dart';
import 'package:spacexmissions/widgets/screens/search_screen_widget.dart';
import 'package:spacexmissions/widgets/screens/splash_screen.dart';

void main() {
  locator.registerSingleton<AppRepositoryProvider>(AppRepositoryProvider());

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<ThemeBloc>(
      create: (context) => ThemeBloc(),
      child: ChangeNotifierProvider.value(
        value: locator<AppRepositoryProvider>(),
        child: Builder(
          builder: (
            context,
          ) {
            final repoProvider = Provider.of<AppRepositoryProvider>(
              context,
            );
            return BlocProvider<MissionSearchBloc>(
              create: (BuildContext context) => MissionSearchBloc(
                  repository: repoProvider.missionRepository,
                  globalVars: repoProvider.globalVars),
              child: OverlaySupport.global(
                toastTheme: ToastThemeData(),
                child: BlocBuilder<ThemeBloc, ThemeState>(
                  builder: (context, state) {
                    return MaterialApp(
                        supportedLocales: S.delegate.supportedLocales,
                        localizationsDelegates: [
                          S.delegate,
                          GlobalMaterialLocalizations.delegate,
                          GlobalWidgetsLocalizations.delegate,
                          GlobalCupertinoLocalizations.delegate,
                        ],
                        theme: ThemeData(
                          primarySwatch: Colors.blue,
                          splashColor: Colors.transparent,
                        ),
                        initialRoute: Routes.splashScreen,
                        onGenerateRoute: (RouteSettings settings) {
                          final arguments = settings.arguments;

                          switch (settings.name) {
                            case Routes.splashScreen:
                              return NoTransitionRoute(
                                builder: (context) => SplashScreen(),
                                settings: settings,
                              );
                            case Routes.searchScreen:
                              return FadeTransitionRoute(
                                builder: (context) => SearchScreenWidget(),
                                settings: settings,
                              );
                            case Routes.gallery:
                              final args = arguments as PicturePagesArgs;

                              return MaterialPageRoute(
                                builder: (context) => Gallery(
                                  urls: args.urls,
                                  initialIndex: args.initialIndex,
                                  noDetailOpen: args.noDetailOpen,
                                ),
                                settings: settings,
                              );
                          }
                        });
                  },
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
